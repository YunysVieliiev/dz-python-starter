frst_var = int(input('Введіть висоту прямокутника : '))
sec_var = int(input('Введіть ширину прямокутника : '))
if frst_var > 0 and sec_var >0:
    for i in range(frst_var):
        for j in range(sec_var):
            print('*', end='')
        print()
elif (frst_var == 0 or sec_var == 0) or (frst_var == 0 and sec_var == 0):
    print('Помилка! Не існує прямокутника  зі стороною = 0')
else:
    print('Помилка! Не існує прямокутника зі стороною < 0')
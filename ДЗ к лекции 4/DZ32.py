frst_var = int(input('Введіть число: '))
total = 1
if frst_var >= 0:
    for i in range(1, frst_var + 1):
        total *= i
    print(f'Факторіал числа {frst_var} дорівнює {total}')
elif frst_var < 0:
    print("Помилка! Факторіалу від'ємного числа не існує!")

# Варіант без циклів
# from math import *

# a = int(input('Введіть число: '))
# print(f'Факторіал числа {a} дорівнює {factorial(a)}')

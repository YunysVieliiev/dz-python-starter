frst_var = int(input('Введіть перше число : '))
sec_var = int(input('Введіть друге число : '))
total = 0
average = (frst_var + sec_var) / 2
for i in range(frst_var, sec_var + 1):
    if average % i == 0:
        total += i
if total > 0:
    print(
        f'Cума всіх натуральних чисел від {frst_var} до {sec_var} , які є кратними середньому арифметичному цього проміжку дорівнює: {total}')
else:
    print(
        f'Натуральних чисел від {frst_var} до {sec_var} , які є кратними середньому арифметичному цього проміжку не існує!')

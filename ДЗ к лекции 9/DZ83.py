value = int(input('Введіть кількість сходинок: '))


def count_moves(value: int) -> int:
    '''Ця функція підраховує кількість способів, якими можна піднятися на задану сходинку'''
    floor = 0
    step = 1
    for i in range(value):
        floor, step = step, floor + step
    return step


print(f'Кількість варіантів для {value} сходинок:', count_moves(value))

value = int(input('Введіть кількість сходинок: '))

#Варіант через рекурсію:
# def count_moves(value: int) -> int:
#     '''Ця функція підраховує кількість способів, якими можна піднятися на задану сходинку'''
#     if value == 0 or value == 1:
#         return 1
#     else:
#         return count_moves(value - 1) + count_moves(value - 2)
#
#
# print(f'Кількість варіантів для {value} сходинок:', count_moves(value))

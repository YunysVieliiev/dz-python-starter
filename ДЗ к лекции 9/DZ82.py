value = input('Введіть слово, яке бажаєте перевірити на паліндромність: ').lower()


def palindrom(value: str) -> str:
    '''Ця функція обчислює, чи є введене значення паліндромом'''
    if value == value[::-1]:
        return 'Ця фраза являється паліндромом'
    else:
        return 'Ця фраза не являється паліндромом'


print(palindrom(value))
#Рішення через рекурсивну функцію
# value = input('Введіть слово, яке бажаєте перевірити на паліндромність: ').lower()
#
#
# def palindrom(value: str) -> str:
#     '''Ця функція обчислює, чи є введене значення паліндромом'''
#     if len(value) <= 1:
#         return 'Ця фраза являється паліндромом'
#     if value[0] != value[-1]:
#         return 'Ця фраза не являється паліндромом'
#     return palindrom(value[1:-1])
#
#
# print(palindrom(value))

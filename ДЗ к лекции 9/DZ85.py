from math import *


def quadratic_equation(first_val, sec_val, third_val):
    '''Ця функція підраховує результати квадратного рівняння'''
    x1 = None
    x2 = None

    def calc_rezult(first_val, sec_val, third_val):
        '''Ця функція підраховує діскриминант квадратного рівняння'''
        D = pow(sec_val, 2) - 4 * first_val * third_val
        return D

    Discr = calc_rezult(first_val, sec_val, third_val)
    if first_val == 0:
        return None
    if Discr > 0:
        x1 = (-sec_val - sqrt(Discr)) / (2 * first_val)
        x2 = (-sec_val + sqrt(Discr)) / (2 * first_val)
        return [x1, x2]
    if Discr == 0:
        x1 = -sec_val / (2 * first_val)
        return x1
    else:
        return None


Result = quadratic_equation(1, 2, 1)

if Result != None:
    print('Відповідь =', Result)
else:
    print('Коренів немає')

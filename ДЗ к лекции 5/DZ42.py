List1 = list(input('Введіть значення елементів першого списку: '))
List2 = list(input('Введіть значення елементів другого списку: '))
List3 = []
for i in List1:
    if i not in List2:
        if i in List3:
            continue
        else:
            List3.append(i)
for j in List2:
    if j not in List1:
        if j in List3:
            continue
        else:
            List3.append(j)
List4 = List3

print('Список з унікальними значеннями вводимих списків та без повторень:', List3)
List3.reverse()
print('Він же у зворотній послідовності:', List3)
List3.sort()
print('Цей же список, відсортований від меньшого значення до більшого:', List3)
List3.sort(reverse=True)
print('Та відсортованний навпаки:', List3)

int_list = []
for i in range(1, int(input('Введіть число, у діапазоні якого буде створено список: ')) + 1):
    int_list.append(i)
new_list = []
for i in int_list:
    if i % 2 != 0:
        new_list.append(i)
int_list.clear()
repeats = int(
    input('Введіть кількість повторень списку, у якому знаходяться тільки непарні числа із цього діапазону: '))
print(f'Результат: {new_list * repeats}')
new_var = int(input('Введіть число, кількість повторів якого треба підрахувати: '))
new_list2 = new_list * repeats
count = 0
if new_var in new_list2:
    print(f'Число {new_var} зустрічається у цьому списку {new_list2.count(new_var)} раз(и)ів.')
    print(f'Перший індекс, за яким знаходиться дане число: {new_list2.index(new_var)}')
else:
    print('Данного числа у цьому списку немає')

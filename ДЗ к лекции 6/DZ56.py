feedback = input('Введіть ваш відгук про відпочинок на курорті «Морська зірка»: ').lower().split()
count_menu = feedback.count('меню')
count_gym = feedback.count('спортзал')
count_service = feedback.count('сервіс')
print(
    f'Кількість згадувань "Меню" у відгуку: {count_menu}\n\
Кількість згадувань "Cпортзала(у)" у відгуку: {count_gym}\n\
Кількість згадувань "Cервіса(у)" у відгуку: {count_service}')
feedback1 = []
for i in feedback:
    for j in list(i):
        feedback1.append(j)
if len(feedback1) > 60:
    print('За ваш детальний відгук, ви отримуєте знижку 15% на наступне відвідування курорту «Морська зірка»!')
else:
    print('Дякуємо вам за відгук, але на знижку ви не настрочили!')

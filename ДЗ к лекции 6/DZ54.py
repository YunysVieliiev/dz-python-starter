from collections import namedtuple

Marks = namedtuple('Marks', 'Алгебра Геометрія Історія Інформатика Географія')

marks1 = Marks(5, 4, 3, 4, 5)
print("У Генадія наступний табель:", marks1)

marks2 = Marks(5, 4, 2, 3, 5)
print("У Єгора наступний табель:", marks2)

marks2 = Marks(1, 2, 3, 4, 5)
print("У Галини наступний табель:", marks2)

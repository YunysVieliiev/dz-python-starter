from math import*
a = float(input('Введіть значення а: '))
b = float(input('Введіть значення b: '))
c = float(input('Введіть значення с: '))
D = pow(b,2) - 4*a*c
if a == 0:
    print("Нема коренів (ділення на 0)")
elif D < 0:
    print("Нема коренів")
elif D == 0:
    x = -b/2*a
    print(f"Відповідь: {x}")
else:
    x1 = (-b + sqrt(D))/(2*a)
    x2 = (-b - sqrt(D))/(2*a)
    print(f"Відповідь: {x1} та {x2}")
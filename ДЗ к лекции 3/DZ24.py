# Операції, які можна тут виконати: +;-;*;/;**(возведіння в ступінь);квадратний корінь;кубічний корінь;cos;sin;tg)
from math import*
first_variable = float(input('Введіть значення першої змінної: '))
operation = str(input('Введіть необхідну вам операцію: '))
if operation == '+' :
    second_variable = float(input('Введіть значення другої змінної: '))
    print('Результат :', first_variable + second_variable)
elif operation == '-':
    second_variable = float(input('Введіть значення другої змінної: '))
    print('Результат :', first_variable - second_variable)
elif operation == '*':
    second_variable = float(input('Введіть значення другої змінної: '))
    print('Результат :', first_variable * second_variable)
elif operation == '/':
    second_variable = float(input('Введіть значення другої змінної: '))
    if second_variable == 0:
        print('Я знав, що ти спробуєш :)) Ділити на 0 не можна!')
    else:
        print('Результат :', first_variable / second_variable)
elif operation == '**':
    second_variable = float(input('Введіть значення другої змінної: '))
    print('Результат :', first_variable ** second_variable)
elif operation == 'квадратний корінь':
    print('Результат :', sqrt(first_variable))
elif operation == 'кубічний корінь':
    print('Результат :', pow(first_variable, 1/3))
elif operation == 'cos':
    print('Результат :', cos(first_variable))
elif operation == 'sin':
    print('Результат :', sin(first_variable))
elif operation == 'tg':
    print('Результат :', tan(first_variable))
import math  # Кожен імпорт має бути на окремому рядку

import random  # Між імпортами має бути пустий рядок

from math import pi, e  # Більш зрозумілий імпорт констант, хоча можно робити так, як спочатку було у файлі

PRINT_1 = 5  # Змінні небажанно називати іменами вбудованих функцій. Також константи бажано називати у верхньому реєстрі
count = int(input(
    'Введите количество повторов: '))  # Звичайні змінні бажано називати нижнім реєстром. Те ж саме стосується початку рядка.
print(PRINT_1 * count)  # Пробіли мають бути з боків операціі, та їх не повинно бути перед і після дужок.
print(pi * PRINT_1 * count)  # Пробіли мають бути з боків операціі, та їх не повинно бути перед і після дужок.
print(e * 2)  # Пробіли мають бути з боків операціі, та їх не повинно бути перед і після дужок.
while PRINT_1 >= 0:  # Нечитабельні пробіли
    PRINT_1 -= 1
str_1 = 'my string'  # Змінні небажанно називати іменами вбудованих функцій.
sum_1 = 0  # Змінні небажанно називати іменами вбудованих функцій.
for elem in str_1:  # Нечитабельні пробіли
    sum_1 += pow(str_1.find(elem), 2)  # Пробіли мають бути з боків операціі
print("sum=",
      sum_1)  # Якщо не задумано виводити на екран кожну операцію, то цей прінт має бути на одному рівні з циклом


# Функція повинна бути обмежена двома пустими рядками зверху та знизу

def my_func(atr=1):  # Назву функціі треба давати у нижньому реєстрі
    print('atr', atr)  # Зайві пробіли у дужках та рядках


my_func(
    atr=5)  # Знову пробіли. Якщо у функцію зашит прінт, то для виводу результату на екран, достатньо визвати функцію.

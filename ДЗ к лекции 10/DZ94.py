def add_to_store() -> str:
    '''Ця функція дає запит до користувача з приводу додавання товарів до інтернет-магазину,
     та результат додає у список у вигляді кортежів'''
    store = []
    while True:
        answer = input('Ви бажаєте додати товар до інтернет магазину (Так/Ні):\n'
                       '')
        if answer.lower() == 'yes' or answer.lower() == 'так' or answer.lower() == 'т' or answer == 'nfr':
            while True:
                answer_1 = input('Введіть назву канцтовару, який хочете додати у інтернет-магазин.'
                                 ' Якщо хочете зупинитися, натисніть "STOP":\n'
                                 '')
                if answer_1.lower() != 'stop' and answer_1.lower() != 'іещз':
                    answer_2 = input('Введіть введіть ціну цього канцтовару:\n'
                                     '')
                    store.append((f'Назва товару: {answer_1}', f'Його ціна: {answer_2}'))
                    print(f'На данний момент інтернет-магазин виглядає наступним чином:\n{store}')
                else:
                    break
        elif answer.lower() == 'no' or answer.lower() == 'ні' or answer.lower() == 'ys' \
                or answer.lower() == 'n' or answer == 'н':
            print(f'Дякую, що завітали до інтернет магазину канцтоварів! Його кінцевий вигляд наступний:\n{store}')
            break
        else:
            print('Я дуже вибачаюсь, повторіть ваш запит знову!')


add_to_store()

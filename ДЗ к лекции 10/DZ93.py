import math


def calc_func() -> float:
    '''Це основна функція калькулятора'''
    print('Доброго дня! Ви завітали до нашого екслюзивного інженерного калькулятора!')
    print(
        'Для початку роботи, введіть порядковий номер операції, яку ви хочете здійснити, з переліку який наведений нижче: ')
    print()
    print('1.Додавання "+"\t\t\t8.sin(X)(в градусах) \t\t15.Факторіал (X!)\t\t22.3√X\n'
          '2.Віднімання "-"\t\t9.cos(X)(в градусах) \t\t16.ln(X)\t\t\t\t23.|X|\n'
          '3.Множення "*"\t\t\t10.tg(X)(в градусах) \t\t17.log(X)\t\t\t\t24.Залишок від ділення на Х\n'
          '4.Ділення "/"\t\t\t11.arcsin(X)\t\t\t\t18.logX(Y)\t\t\t\t25.10^X\n'
          '5.sin(X)(в радіанах)\t12.arccos(X)\t\t\t\t19.X^2\t\t\t\t\t26.Гіпотенуза трикутника з катетами Х та У\n'
          '6.cos(X)(в радіанах)\t13.arctg(X)\t\t\t\t\t20.X^3\t\t\t\t\t27.Вивести на екран π\n'
          '7.tg(X)(в радіанах) \t14.e^X\t\t\t\t\t\t21.√X\t\t\t\t\t28.Вивести на екран e')
    print()
    while True:
        operation = input('Введіть порядковий номер операції: ')
        if operation.isdigit() == True and 1 <= int(operation) <= 28:
            if operation == '1':
                print(addition())
            elif operation == '2':
                print(subtraction())
            elif operation == '3':
                print(multiply())
            elif operation == '4':
                print(division())
            elif operation == '5':
                print(sin_rad())
            elif operation == '6':
                print(cos_rad())
            elif operation == '7':
                print(tg_rad())
            elif operation == '8':
                print(sin_ang())
            elif operation == '9':
                print(cos_ang())
            elif operation == '10':
                print(tg_ang())
            elif operation == '11':
                print(arcsin_rad())
            elif operation == '12':
                print(arccos_rad())
            elif operation == '13':
                print(arctg_rad())
            elif operation == '14':
                print(exp())
            elif operation == '15':
                print(fact())
            elif operation == '16':
                print(ln())
            elif operation == '17':
                print(log_10())
            elif operation == '18':
                print(log_XY())
            elif operation == '19':
                print(square())
            elif operation == '20':
                print(qube())
            elif operation == '21':
                print(root_square())
            elif operation == '22':
                print(root_qube())
            elif operation == '23':
                print(module_abs())
            elif operation == '24':
                print(division_1())
            elif operation == '25':
                print(ten_pow())
            elif operation == '26':
                print(hyp())
            elif operation == '27':
                print(math_pi())
            elif operation == '28':
                print(math_e())
            break
        else:
            print('Цей калькулятор має лише 28 кнопок, які є ЧИСЛАМИ!!!')


def addition() -> float:
    '''Ця функція виконує операцію додавання'''
    while True:
        first_variable = input('Введіть значення першого доданку: ')
        second_variable = input('Введіть значення другого доданку: ')
        if first_variable.isdigit() == True and second_variable.isdigit() == True \
                or '-' in first_variable or '-' in second_variable or '.' in first_variable or '.' in second_variable:
            return f'Результат : {float(first_variable) + float(second_variable)}'
        else:
            print('Вводити можна тільки цифри!')


def subtraction() -> float:
    '''Ця функція виконує операцію віднімання'''
    while True:
        first_variable = input('Введіть значення зменьшуванного: ')
        second_variable = input('Введіть значення від\'ємника: ')
        if first_variable.isdigit() == True and second_variable.isdigit() == True \
                or '-' in first_variable or '-' in second_variable or '.' in first_variable or '.' in second_variable:
            return f'Результат : {float(first_variable) - float(second_variable)}'
        else:
            print('Вводити можна тільки цифри!')


def multiply() -> float:
    '''Ця функція виконує операцію множення'''
    while True:
        first_variable = input('Введіть значення 1го множника: ')
        second_variable = input('Введіть значення 2го множника: ')
        if first_variable.isdigit() == True and second_variable.isdigit() == True \
                or '-' in first_variable or '-' in second_variable or '.' in first_variable or '.' in second_variable:
            return f'Результат : {float(first_variable) * float(second_variable)}'
        else:
            print('Вводити можна тільки цифри!')


def division() -> float:
    '''Ця функція виконує операцію ділення'''
    while True:
        first_variable = input('Введіть значення діленого: ')
        second_variable = input('Введіть значення дільника: ')
        if first_variable.isdigit() == True and second_variable.isdigit() == True \
                or '-' in first_variable or '-' in second_variable or '.' in first_variable or '.' in second_variable:
            if second_variable == '0':
                return 'Я знав, що ти спробуєш :)) Ділити на 0 не можна!'
            else:
                return f'Результат : {float(first_variable) / float(second_variable)}'
        else:
            print('Вводити можна тільки цифри!')


def sin_rad() -> float:
    '''Ця функія підраховує sin у радіанах'''
    while True:
        first_variable = input('Введіть значення у радіанах: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {math.sin(float(first_variable))}'
        else:
            print('Вводити можна тільки цифри!')


def cos_rad() -> float:
    '''Ця функія підраховує cos у радіанах'''
    while True:
        first_variable = input('Введіть значення у радіанах: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {math.cos(float(first_variable))}'
        else:
            print('Вводити можна тільки цифри!')


def tg_rad() -> float:
    '''Ця функія підраховує tg у радіанах'''
    while True:
        first_variable = input('Введіть значення у радіанах: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {math.tan(float(first_variable))}'
        else:
            print('Вводити можна тільки цифри!')


def sin_ang() -> float:
    '''Ця функія підраховує sin у градусах'''
    while True:
        first_variable = input('Введіть значення у градусах: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {math.sin(math.radians(float(first_variable)))}'
        else:
            print('Вводити можна тільки цифри!')


def cos_ang() -> float:
    '''Ця функія підраховує cos у градусах'''
    while True:
        first_variable = input('Введіть значення у градусах: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {math.cos(math.radians(float(first_variable)))}'
        else:
            print('Вводити можна тільки цифри!')


def tg_ang() -> float:
    '''Ця функія підраховує tg у градусах'''
    while True:
        first_variable = input('Введіть значення у градусах: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {math.tan(math.radians(float(first_variable)))}'
        else:
            print('Вводити можна тільки цифри!') or '-' in first_variable


def arcsin_rad() -> float:
    '''Ця функія підраховує arcsin у радіанах'''
    while True:
        first_variable = input('Введіть значення у радіанах: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            while True:
                if -1 <= float(first_variable) <= 1:
                    return f'Результат : {math.asin(float(first_variable))}'
                else:
                    print('Помилка! Арксінус у радіанах може приймати значення тільки від -1 до 1 ')
                    first_variable = input('Введіть значення у радіанах: ')
        else:
            print('Вводити можна тільки цифри!')


def arccos_rad() -> float:
    '''Ця функія підраховує arccos у радіанах'''
    while True:
        first_variable = input('Введіть значення у радіанах: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            while True:
                if -1 <= float(first_variable) <= 1:
                    return f'Результат : {math.acos(float(first_variable))}'
                else:
                    print('Помилка! Арккосінус у радіанах може приймати значення тільки від -1 до 1 ')
                    first_variable = input('Введіть значення у радіанах: ')
        else:
            print('Вводити можна тільки цифри!')


def arctg_rad() -> float:
    '''Ця функія підраховує arctg у радіанах'''
    while True:
        first_variable = input('Введіть значення у радіанах: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {math.atan(float(first_variable))}'
        else:
            print('Вводити можна тільки цифри!')


def exp() -> float:
    '''Ця функія підраховує екпоненту в заданому ступені'''
    while True:
        first_variable = input('Введіть значення у ступінь якого ви хочете возвести експоненту: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {math.exp(float(first_variable))}'
        else:
            print('Вводити можна тільки цифри!')


def fact() -> int:
    '''Ця функія підраховує факторіал числа'''
    while True:
        first_variable = input('Введіть число, факторіал якого треба підрахувати: ')
        if first_variable.isdigit() == True:
            return f'Результат : {math.factorial(int(first_variable))}'
        else:
            print('Вводити можна тільки натуральні, невід\'ємні числа!')


def ln() -> float:
    '''Ця функія підраховує натуральний логарифм числа'''
    while True:
        first_variable = input('Введіть число, натуральний логарифм якого треба підрахувати: ')
        if first_variable.isdigit() == True and float(first_variable) != 0 or '.' in first_variable:
            return f'Результат : {math.log1p(float(first_variable))}'
        else:
            print('Вводити можна тільки невід\'ємні числа та ненульові числа!')


def log_10() -> float:
    '''Ця функія підраховує логарифм числа з основою 10'''
    while True:
        first_variable = input('Введіть число, десятичний логарифм якого треба підрахувати: ')
        if first_variable.isdigit() == True and float(first_variable) != 0 or '.' in first_variable:
            return f'Результат : {math.log10(float(first_variable))}'
        else:
            print('Вводити можна тільки невід\'ємні числа та ненульові числа!')


def log_XY() -> float:
    '''Ця функія підраховує логарифм числа X основою Y'''
    while True:
        first_variable = input('Введіть число, логарифм якого треба підрахувати: ')
        second_variable = input('Введіть значення основи логарифму: ')
        if second_variable != '0' and second_variable != '1' and first_variable.isdigit() == True \
                and second_variable.isdigit() == True \
                or '.' in first_variable or '.' in second_variable:
            return f'Результат : {math.log(float(first_variable), float(second_variable))}'
        else:
            print('Вводити можна тільки невід\'ємні числа та ненульові числа! Також основою логарифму не може бути 1!')


def square() -> float:
    '''Ця функія підраховує квадрат числа'''
    while True:
        first_variable = input('Введіть число, яке треба возвести в квадрат: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {pow(float(first_variable), 2)}'
        else:
            print('Вводити можна тільки цифри!')


def qube() -> float:
    '''Ця функія підраховує куб числа'''
    while True:
        first_variable = input('Введіть число, яке треба возвести в куб: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {pow(float(first_variable), 3)}'
        else:
            print('Вводити можна тільки цифри!')


def root_square() -> float:
    '''Ця функія підраховує куб числа'''
    while True:
        first_variable = input('Введіть число, квадратний корінь якого треба підрахувати: ')
        if first_variable.isdigit() == True or '.' in first_variable:
            return f'Результат : {pow(float(first_variable), 0.5)}'
        else:
            print('Вводити можна тільки невід\'ємні числа!')


def root_qube() -> float:
    '''Ця функія підраховує куб числа'''
    while True:
        first_variable = input('Введіть число, кубічний корінь якого треба підрахувати: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            if float(first_variable) < 0:
                return f'Результат : {pow(abs(float(first_variable)), 1 / 3) * (-1)}'
            else:
                return f'Результат : {pow(float(first_variable), 1 / 3)}'
        else:
            print('Вводити можна тільки цифри')


def module_abs() -> float:
    '''Ця функія виводить модуль числа'''
    while True:
        first_variable = input('Введіть число, модуль якого треба вивести: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {math.fabs(float(first_variable))}'
        else:
            print('Вводити можна тільки цифри')


def division_1() -> float:
    '''Ця функція виводить остаток від ділення першої змінної на другу'''
    while True:
        first_variable = input('Введіть значення діленого: ')
        second_variable = input('Введіть значення дільника: ')
        if first_variable.isdigit() == True and second_variable.isdigit() == True \
                or '-' in first_variable or '-' in second_variable or '.' in first_variable or '.' in second_variable:
            if second_variable == '0':
                return 'Я знав, що ти спробуєш :)) Ділити на 0 не можна!'
            else:
                return f'Результат : {float(first_variable) % float(second_variable)}'
        else:
            print('Вводити можна тільки цифри!')


def ten_pow() -> float:
    '''Ця функія підраховує 10 у ступені Х'''
    while True:
        first_variable = input('Введіть число, в ступінь якого треба возвести 10: ')
        if first_variable.isdigit() == True or '-' in first_variable or '.' in first_variable:
            return f'Результат : {pow(10, float(first_variable))}'
        else:
            print('Вводити можна тільки цифри!')


def hyp() -> float:
    '''Ця функція підраховує значення гіпотенузи з заданими катетами'''
    while True:
        first_variable = input('Введіть значення першого катету: ')
        second_variable = input('Введіть значення другого катету: ')
        if first_variable.isdigit() == True and second_variable.isdigit() == True \
                or '.' in first_variable or '.' in second_variable:
            return f'Результат : {math.hypot(float(first_variable), float(second_variable))}'
        else:
            print('Вводити можна невід\'ємні числа!')


def math_pi() -> float:
    '''Ця функція виводить на екран число π'''
    return f'Число π дорівнює: {math.pi}'


def math_e() -> float:
    '''Ця функція виводить на екран число e'''
    return f'Число е дорівнює: {math.e}'


calc_func()

while True:
    print('Ви бажаєте продовжити роботу з інженерним калькулятором? (Так/Ні):')
    answer_1 = input()
    if answer_1.lower() == 'так' or answer_1.lower() == "т" or answer_1.lower() == 'yes' or answer_1.lower() == 'y' \
             or answer_1.lower() == 'nfr' or answer_1.lower() == 'нуі' :
        calc_func()
    elif answer_1.lower() == 'ні' or answer_1.lower() == "н" or answer_1.lower() == 'no' or answer_1.lower() == 'ys'\
            or answer_1.lower() == 'тщ' or answer_1.lower() == 'n':
        print('Дякуємо, що використовували наш інженерний калькулятор. Ще побачимось!')
        break
    else:
        print('Вибачте, чи не можете ви відповісти більш зрозуміло?')

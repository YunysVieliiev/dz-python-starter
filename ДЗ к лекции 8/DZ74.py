def average_of_three_var(var_1, var_2, var_3):
    return (var_1 + var_2 + var_3) / 3


def main():
    var_1 = int(input('Введіть перше значення: '))
    var_2 = int(input('Введіть друге значення: '))
    var_3 = int(input('Введіть третє значення: '))
    print('Середнє арифметичне цих чисел дорінює:', average_of_three_var(var_1, var_2, var_3))


main()

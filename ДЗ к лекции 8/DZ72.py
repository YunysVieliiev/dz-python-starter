def function1(var_1):
    return var_1 * 2


def function2(var_2):
    return var_2 ** 2


i = -5
while i <= 5:
    y = function1(i)
    i += 0.5
    print(float(y))

print()

i = -5
while i <= 5:
    y = function2(i)
    i += 0.5
    print(float(y))

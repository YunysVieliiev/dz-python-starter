def hello(name='Yunys'):
    print('Hello, ', name, '!', sep='')


name_1 = (input('Введіть ваше ім\'я: '))
if len(name_1) != 0:
    hello(name_1)
else:
    hello()

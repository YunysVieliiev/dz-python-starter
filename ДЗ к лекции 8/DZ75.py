def index_height_weight(height,weight):
    index_height_weight=weight/((height)**2)
    if index_height_weight<18.5:
        return 'Увага! Недостатня вага! Вам треба терміново поїхати до бабусі!'
    elif 18.5<=index_height_weight<24.9:
        return 'Маса тіла в нормі. Ви молодець!'
    else:
        return'Увага! У вас надмірна вага! Саме тому ми маємо для вас пропозицію на абонемент у спортзал,\
 на який діє знижка 5%, якщо ви приведете із собою такого ж друга!'


def main():
    switch_on_off=input("Введіть ваше імя, якщо хочете дізнатися, наскільки все погано,\
 або введіть off, щоб закінчити програму: ")
    while switch_on_off:
        if switch_on_off=='off':
            break
        else:
            height=float(input("Введіть ваш зріст (у метрах) :"))
            weight=float(input("Введіть вашу вагу (у кілограмах) :"))
            print(index_height_weight(height,weight))
            print()
            switch_on_off = input("Введіть ваше імя, якщо хочете дізнатися, наскільки все погано,\
 або введіть off, щоб закінчити програму: ")

main()
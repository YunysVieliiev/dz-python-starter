import operator
from collections import OrderedDict
dict_1 = {'Кінг': ['Воно'], 'Лондон': ['Білий клик'], 'Керролл': ['Аліса у країні чудес']}
author = input('Введіть автора. Якщо хочете зупинитися - натисніть f:')

while author != 'f':
    books = list()
    book = input('Введіть твір. Якщо хочете зупинитися - натисніть g:')
    while book != 'g':
        if book not in books:
            books.append(book)
        else:
            continue
        book = input('Введіть твір. Якщо хочете зупинитися - натисніть g:')
    dict_1[author] = books
    author = input('Введіть автора. Якщо хочете зупинитися - натисніть f:')

wish_1=input('Якщо ви хочете відсортувати словник за твором та вивести його на екран - наберіть "Твір"\n\
Якщо ви хочете відсортувати словник за автором та вивести його на екран - наберіть "Автор"\n\
Якщо ви не хочете сортувати словник, наберіть все що завгодно, окрім "Твір" та "Автор": ')
if wish_1=='Твір':
    sorted_values = sorted(dict_1.values())
    new_sorted_dict = {}
    for i in sorted_values:
        for k in dict_1.keys():
            if dict_1[k] == i:
                new_sorted_dict[k] = dict_1[k]
                break
    print(new_sorted_dict)
elif wish_1=='Автор':
   print(sorted(dict_1.items()))
else:
    print(dict_1)
workers = {
    'Смирнов': {
        'посада': 'менеджер з продажу',
        'ефективність': 86,
        'досвід роботи': 10,
        'портфоліо': 'велике',
        'коефіцієнт ефективності': 10,
        'стек технологій': 'маленький',
        'зарплата': 10000
    },
    'Федорів': {
        'посада': 'директор',
        'ефективність': 90,
        'досвід роботи': 15,
        'портфоліо': 'велике',
        'коефіцієнт ефективності': 15,
        'стек технологій': 'великий',
        'зарплата': 100000
    },
    'Боброва': {
        'посада': 'секретар',
        'ефективність': 70,
        'досвід роботи': 3,
        'портфоліо': 'маленьке',
        'коефіцієнт ефективності': 6,
        'стек технологій': 'маленький',
        'зарплата': 7000
    },
}

name = input('Введіть ім\'я потрібного вам співробітника. Якщо хочете зупинитись, натисніть stop: ')
while name != 'stop':
    print('На даний момент показники цього співробітника наступні:')
    for key, value in workers[name].items():
        print(key, ':', value)
    changes = input('Оберіть характеристику, яку ви хочете змінити. Якщо хочете зупинитися - натисніть Стоп: ')
    while changes != 'Стоп':
        if changes.lower() == 'посада':
            workers[name]['посада'] = input('Введіть нову посаду цього співробітника: ')
        elif changes.lower() == 'ефективність':
            workers[name]['ефективність'] = input('Введіть нову ефективність цього співробітника: ')
        elif changes.lower() == 'досвід роботи':
            workers[name]['досвід роботи'] = input('Введіть новий показник досвіду роботи цього співробітника: ')
        elif changes.lower() == 'портфоліо':
            workers[name]['портфоліо'] = input('Введіть нове портфоліо цього співробітника: ')
        elif changes.lower() == 'коефіцієнт ефективності':
            workers[name]['коефіцієнт ефективності'] = input('Введіть новий коефіцієнт ефективності'
                                                             ' цього співробітника: ')
        elif changes.lower() == 'стек технологій':
            workers[name]['стек технологій'] = input('Введіть новий показник стеку технологій '
                                                     'цього співробітника: ')
        elif changes.lower() == 'зарплата':
            workers[name]['зарплата'] = input('Введіть нову зарплату цього співробітника: ')
        changes = input('Оберіть характеристику, яку ви хочете змінити. Якщо хочете зупинитися - натисніть Стоп: ')
    name = input('Введіть ім\'я потрібного вам співробітника. Якщо хочете зупинитись, натисніть stop: ')

wish_1 = input(
    'Якщо ви хочете відсортувати бібліотеку працівників за прізвищем та вивести її на екран - наберіть "Прізвище"\n\
Якщо ви хочете відсортувати бібліотеку працівників за показником ефективності та вивести її на екран -'
    ' наберіть "Ефективність"\n\
Якщо ви не хочете сортувати бібліотеку, наберіть все що завгодно, окрім "Прізвище" та "Автор": ')
if wish_1 == 'Ефективність':
    lst = []
    for worker in sorted(list(workers)):
        lst.append(str(workers[worker]['ефективність']) + ' ' + worker)
    print(sorted(lst, reverse=True))
elif wish_1 == 'Прізвище':
    keys = list(workers.keys())
    print(sorted(keys))
else:
    print('Дякуємо, що користувалися нашою бібліотекою! Приходьте ще!')
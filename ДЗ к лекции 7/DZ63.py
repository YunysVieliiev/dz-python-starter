def lists_2(List1, List2):
    List3 = []
    for i in List1:
        if i not in List2:
            if i in List3:
                continue
            else:
                List3.append(i)
    for j in List2:
        if j not in List1:
            if j in List3:
                continue
            else:
                List3.append(j)
    return List3


print('Список з унікальними значеннями:', lists_2([1, 2, 3, 4, 5, 5, 5, 4, 3], [4, 5, 6, 7, 8, 9, 9, 8, 7, 6]))
